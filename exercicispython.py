#TIPUS BÀSIC

#1. Escriu un programa que demani dos nombres i que retorni la seva mitjana aritmètica.
'''
num1 = int(input("Number 1: "))
num2 = int(input("Number 2: "))
print((num1 + num2) / 2)
'''

#2. Escriu un programa que demani una distància en peus i polzades i que escrigui
#aquesta distància en centímetres. Recorda que un peu son 12 polsades i una polzada
#són 2.54 cm
'''
peus = int(input("Distància en peus: "))
polsades = int(input("Distància en polsades: "))
print("Distància = ", (12*peus + polsades)*2.54)
'''

#3. Escriu un programa que demani una temperatura en graus Celsius i que escrigui la
#temperatura en graus Fahrenheit. Recorda que la relació entre graus Celsius (C) i
#Fahrenheit (F) és la següent F – 32 = 1,8 * C
'''
grausC = int(input("Temperatura en graus Celsius: "))
print("Temperatura en graus Fahrenheit: ", grausC * 1.8 + 32)
'''

#4. Escriu un programa que demani una quantitat de segons i que escrigui quants minuts i
#segons són.
'''
tempsSegons = int(input("Temps en segons: "))
#print("Temps en minuts i segons: ", divmod(tempsSegons, 60))
print("Temps en minuts i segons:", int(tempsSegons / 60), "minuts i", tempsSegons % 60, "segons.")
'''

#EXERCICIS DE MANIPULACIONS DE COL·LECCIONS

#5. Escriu un programa que creï una llista amb les vocals.
#   a. Que demani a l'usuari quin número d’element de la llista vol modificar i amb quin
#   valor, i mostri per pantalla la llista modificada.
#   b. Després que demani quina lletra vol modificar i amb quin valor, i mostri per
#   pantalla la llista modificada.

'''
vocals = ["a", "e", "i", "o", "u"]
numModif = int(input("Quin valor vols modificar?: "))
numToModif = input("A quina lletra ho vols modificar?: ")
vocals[numModif] = numToModif
print(vocals)
'''

#6. Escriu un programa que mostri la taula de multiplicar del 7 a partir d'una llista. Cal que
#definiu una llista en la que es guardin els valors de la taula de multiplicar.

'''
taula7 = [i * 7 for i in range(11)]
print(taula7)
'''

#7. Escriu un programa amb un diccionari que relacioni el nom de 4 alumnes i la nota que
#han obtingut en un exercici.
#   a. Que demani a l’usuari de quin alumne vol saber la nota, i que mostri el resultat
#   per pantalla

'''notes = {"Shinji": 7, "Asuka": 9.5, "Rei": 10, "Suzuhara": 2}
nameConsult = input("De quin alumne vols comprovar la nota?: ")
for key in notes.keys():
    if key == nameConsult:
        print(notes.get(nameConsult))'''


#8. Escriu un programa en el que donades dues llistes de paraules (definides internament
#en el programa i sense repeticions) escrigui les següents llistes (sense repeticions)
#   ● Llista de paraules que apareixen tant en la primera llista com en la segona
#   (intersecció)
#   ● Llista de paraules que apareixen en la primera llista, però no en la segona
#   (diferència)
#   ● Llista de paraules que apareixen en la segona llista, però no en la primera
#   ● Llista de paraules que apareixen en alguna de las dues llistes (unió)

'''lista1 = ["Hola", "Sentir", "Pain", "Desesperación", "Desagradable"]
lista2 = ["Hola", "Quemar", "La escuela", "Destrucción", "Desesperación"]
print(lista1)
print(lista2)
interseccions = []
diferencia1 = []
diferencia2 = []
unio = lista1

for paraula in lista1:
    if paraula in lista2:
        interseccions.append(paraula)
    else:
        diferencia1.append(paraula)
for paraula in lista2:
    if paraula not in lista1:
        diferencia2.append(paraula)
for paraula in lista2:
    if (paraula not in lista1):
        unio.append(paraula)
#unio.extend(lista1)
#unio.extend(lista2)

print("Paraules en ambdues: ", interseccions)
print("Paraules només en la primera: ", diferencia1)
print("Paraules només en la segona: ", diferencia2)
print("Paraules en qualsevol: ", unio)'''

#9. Escriu un programa que demani dos nombres enters i que calculi la seva divisió,
#escrivint si la divisió és exacta o no. Has de controlar el cas que indiqui que no es pot
#dividir per 0.

'''num1 = int(input("Nombre 1: "))
num2 = int(input("Nombre 2: "))
if num2 == 0:
    print("Indeterminació")
elif num1 % num2 == 0:
    print("Divisió exacta = ", num1 / num2)
else:
    print("Divisió no exacta = ", num1 / num2)'''

#10. Escriu un programa que pregunti primer si es vol calcular l'àrea d'un triangle o la d'un
#cercle. Si es contesta que es vol calcular l'àrea d'un triangle, el programa ha de
#demanar aleshores la base i l'alçada i escriure l'àrea. Si es contesta que es vol calcular
#l'àrea del cercle, el programa aleshores ha de demanar el radi i escriure l'àrea.

'''opcio = input("Vols calcular l'àrea d'un 'triangle' o d'un 'cercle'?: ")
if opcio == 'triangle':
    base = int(input("Base del triangle?: "))
    altura = int(input("Altura del triangle?: "))
    print("L'àrea del triangle és = ", base * altura / 2)
elif opcio == 'cercle':
    radi = int(input("Radi del cercle?: "))
    print("L'àrea del cercle és = ", radi ** 2 * 3.14)
else:
    print("No has escollit una opció vàlida.")'''

#11. Escriu un programa per repartir una quantitat d'euros en el nombre de bitllets i monedes
#corresponents, tenint en compte un algoritme que ens asseguri el menor nombre de
#bitllets/monedes (és a dir, sempre que puguem utilitzar un bitllet de 10€, no utilitzar-ne
#dos de 5€ ni cinc monedes de 2€)
#   a. Suposa que no existeixen els bitllets majors de 50 € i que les quantitats
#   introduïdes són enteres.

'''diners = int(input("Quants diners vols convertir a bitllets i monedes?: "))
#llistaDiners = {50 : 0, 20 : 0, 10 : 0, 5 : 0, 2 : 0, 1 : 0, 0.5 : 0, 0.2 : 0, 0.1 : 0, 0.05 : 0, 0.02 : 0, 0.01 : 0}      No hi ha cèntims perquè és un int
llistaDiners = {50 : 0, 20 : 0, 10 : 0, 5 : 0, 2 : 0, 1 : 0}

while (diners != 0):
    for key in llistaDiners.keys():
        while True:
            if key <= diners:
                diners -= key
                llistaDiners[key] = llistaDiners.get(key) + 1
                continue
            else:
                break

print("Bitllets o monedes utilitzades (Quantitat del bitllet o moneda, Cops utilitzat)", llistaDiners)'''

#12. Escriu un programa que llegeixi una data (dia, mes i any), determini si la data correspon
#a un valor vàlid i ho escrigui. Les dates seran tres dades de tipus enter que
#correspondran a un dia, un mes i un any (dd, mm, aa).
#   a. Els mesos 1, 3, 5, 7, 8, 10 i 12 tenen 31 dies.
#   b. Els mesos 4, 6, 9 i 11 tenen 30 dies.
#   c. El mes 2 té 28 dies, excepte quan l'any és de traspàs, que té 29 dies.
#   d. Són de traspàs els anys que són múltiples de 400 i els anys que són múltiples de
#   4 però no de 100.

'''data = input("Entra la data, el dia i el mes separats per una coma com al següent exemple: 30, 01, 2022: ")
data = data.split(",")
dd = data[0].strip()
mm = data[1].strip()
aa = data[2].strip()
traspas = False
if dd.isdigit() and mm.isdigit() and aa.isdigit:
    dd = int(dd)
    mm = int(mm)
    aa = int(aa)
    if (mm == 1 or mm == 3 or mm == 5 or mm == 7 or mm == 8 or mm == 10 or mm == 12) and dd <= 31 or ((mm == 2 or mm == 4 or mm == 6 or mm == 9 or mm == 11) and dd <= 30):
        if (mm == 2):
            if (aa % 400 == 0 or (aa % 100 != 0 and aa % 4 == 0)):
                traspas = True
            if (traspas and dd <= 29 or not traspas and dd <= 28):
                print("La data introduida es vàlida")
            else:
                print("El mes de febrer no té", dd, "dies en un any on traspàs és:",traspas)
        else:
            print("La data introduida es vàlida.")
    else:
        print("El mes", mm, "no té", dd, "dies.")
else:
    print("Entrada incorrecta.")'''

#13. Escriu un programa que demani dos nombres enters i escrigui la suma de tots els enters
#des del primer nombre fins al segon. Comprova que el segon nombre és més gran que
#el primer

'''num1 = int(input("Primer número?: "))
num2 = int(input("Segon número?: "))
if (num1 < num2):
    total = 0
    for n in range(num1, num2 + 1):
        total += n
    print("La suma total és =", total)
else:
    print("El primer número ha de ser més petit que el segon.")'''

#14. Escriu un programa que demani un nombre enter major que 0 i que calculi el seu
#factorial

'''num1 = int(input("Entra un número per fer el factorial d'aquest: "))
if num1 > 0:
    total = 1
    for n in range(1, num1 + 1):
        total *= n
    print("El factorial de", num1,"és = ",total)
else:
    print("El número ha de ser més gran que 0, tot i que el factorial de 0 és igual a 1.")'''

#15. Escriu un programa que mostri els múltiples de 7 que hi ha entre els números del 0 al
#100. Fes-ho amb una estructura for.

'''for x in range(0, 100 + 1):
    if (x % 7 == 0):
        print(x)'''

#16. Escriu un programa que permeti crear una llista de paraules. El programa haurà de
#demanar un nombre i a continuació sol·licitar tantes paraules com hagi indicat l'usuari.
#Finalment mostrarà la llista creada

'''llistaParaules = []
totalParaules = int(input("Quantes paraules vols introduir?: "))
i = 0
while(i < totalParaules):
    llistaParaules.append(input("Entra la paraula nº " + str(i + 1) + ": "))
    i += 1
print(llistaParaules)'''

#17. Escriu un programa que permeti crear dues llistes de paraules i que, a continuació,
#elimini de la primera llista les paraules de la segona llista.

'''llistaParaules = []
totalParaules = int(input("Quantes paraules vols introduir?: "))
i = 0
while(i < totalParaules):
    llistaParaules.append(input("Entra la paraula nº " + str(i + 1) + ": "))
    i += 1
print("Llista 1:",llistaParaules)

llistaParaules2 = []
totalParaules = int(input("Quantes paraules vols introduir?: "))
i = 0
while(i < totalParaules):
    llistaParaules2.append(input("Entra la paraula nº " + str(i + 1) + ": "))
    i += 1
print("Llista 2:",llistaParaules2)

for paraula in llistaParaules2:
    if paraula in llistaParaules:
        llistaParaules.remove(paraula)

print("Llista final:",llistaParaules)'''

#18. Escriu un programa que donat un número comprovi si és primer. (for/else)

'''num = int(input("Entra un número per comprovar si és primer: "))

for n in range(2, int(num / 2 + 1)):
    if num % n == 0:
        print("El número no és primer.")
        break
else:
    print("El número és primer.")'''

#19. Escriu els nombres enters positius narcisistes de tres xifres. Els nombres narcisistes de
#tres xifres són de la forma: abc=a^3+b^3+c^3

'''for n in range (100, 1000):
    nums = map(int,str(n))
    total = 0
    for number in nums:
        total += number ** 3
    if (total == n):
        print(n)'''

#20. Donats dos nombres enters n1 i n2 amb n1 < n2, escriu per pantalla tots els nombres
#enters dins l’interval [n1, n2] en ordre creixent.

'''num1 = int(input("Num 1: "))
num2 = int(input("Num 2: "))
if (num1 < num2):
    for n in range (num1, num2 + 1):
        print(n)
else:
    print("El número 1 ha de ser menor que el número 2")'''

#21. Donats dos nombre enters n1 i n2 amb n1 < n2 , escriu tots els nombres enters dins
#l’interval [n1, n2] en ordre decreixent.

'''num1 = int(input("Num 1: "))
num2 = int(input("Num 2: "))
if (num1 < num2):
    for n in reversed(range(num1, num2 + 1)):
        print(n)
else:
    print("El número 1 ha de ser menor que el número 2")'''

#22. Donats dos nombre enters n1 i n2 amb n1 < n2, escriu les arrels quadrades dels
#nombres enters dins l'interval [n1, n2] en ordre creixent.

'''import math
num1 = int(input("Num 1: "))
num2 = int(input("Num 2: "))
if (num1 < num2):
    for n in range(num1, num2 + 1):
        print("Arrel de", n, "=", math.sqrt(n))
else:
    print("El número 1 ha de ser menor que el número 2")'''

#23. Donats dos nombre enters n1 i n2 amb n1 < n2, escriu els nombres enters parells que hi
#ha dins l’interval [n1, n2] en ordre creixent. El nombre zero es considera parell.

'''num1 = int(input("Num 1: "))
num2 = int(input("Num 2: "))
if (num1 < num2):
    for n in range(num1, num2 + 1):
        if (n % 2 == 0):
            print(n)
else:
    print("El número 1 ha de ser menor que el número 2")'''

#24. Escriure els 10 primers múltiples de 2

'''print("Primers 10 múltiples de 2")
for n in range(10):
    x = 2 * n
    print(x)'''

#25. Escriure els n primers múltiples de m

'''n = int(input("Quants múltiples vols?: "))
m = int(input("De quin número?: "))
for a in range(n):
    x = m * a
    print(x)'''

#26. Escriu un programa que donada una paraula ens digui si és “alfabètica”. Una paraula
#és “alfabètica” si totes les seves lletres estan ordenades alfabèticament.
#Per exemple, “amor“, “ceps“ e “himno“ són paraules “alfabètiques”.

'''paraula = input("Introdueix una paraula: ")
if (paraula.isalpha()):
    llistaLletres = list(paraula)
    llistaOrdenada = list(paraula)
    llistaOrdenada.sort()
    check = True
    i = 0
    print(llistaLletres)
    print(llistaOrdenada)
    while (i < len(llistaLletres)):
        if (llistaLletres[i] != llistaOrdenada[i]):
            check = False
            break
        i += 1
    if check:
        print("La paraula és alfabètica.")
    else:
        print("La paraula no és alfabètica.")
else:
    print("La paraula introduida no és vàlida.")'''

#27. Escriu un programa que donada una cadena ens digui si és un palíndrom o no. Una
#frase o cadena és palíndroma si es pot llegir en ordre o a l'inrevés obtenint la mateixa
#frase, per exemple: "dabale arroz a la zorra el abad", és palíndroma.

'''frase = input("Introdueix un text: ")

text = [x for x in frase if not x.isspace()]
textInv = list(reversed(text))

if(text == textInv):
	print("És un palindrom")
else:
	print("No és un palindrom")'''

#28. Escriu un programa que donat un DNI, el verifiqui comprovant que la lletra és correcta
#L’última lletra del DNI es pot calcular a partir dels seus números. Per a això només has
#de dividir el nombre per 23 i quedar-te amb la resta. La resta és un nombre entre 0 i 22.
#La lletra que correspon a cada nombre la tens en aquesta taula:

'''dni = input("Introdueix un dni: ")
lletres = ["T", "R", "W", "A", "G", "M", "I", "F", "P", "D", "X", "B", "N", "J", "Z", "S", "Q", "V", "H", "L", "C", "K", "E"]
residu = int(dni[0:-1]) % 23
lletra = dni[-1].upper()

if(lletra == lletres[residu]):
    print("El dni introduit",dni,"és vàlid.")
else:
    print("El dni introduit",dni,"no és vàlid.")'''

#29. Escriu un programa que donat un fitxer de text d'entrada, el llegeix i mostra la llista de
#paraules ordenades alfabèticament. Les paraules no poden estar repetides, i no han de
#ser keysensitive. És a dir, a la llista només hauria d’aparèixer un cop la paraula “hola” si
#en el fitxer posés “Hola, hola, hoLA ...”

fitxerALlegir = open("C:\Users\34667\Downloads\fitxer.txt", "r")
paraules = []

for linea in fitxerALlegir:
    for paraula in linea:
        if (paraula.lower() not in paraules):
            paraules.append(paraula)

print("Paraules al fitxer:", paraules)

fitxerALlegir.close()
